### kakoune-racket

---

[Racket](https://racket-lang.org/) support for the [Kakoune](https://kakoune.org/) editor.


![](/res/kakoune-racket.png)


#### Install  

---


```
$ mv racket.kak ~/.config/kak/autoload/
```

[plug.kak](https://github.com/robertmeta/plug.kak)  
```
plug "KJ_Duncan/kakoune-racket.kak" domain "bitbucket.org"
```

Supported file extensions: `(rkt|rktd|rktl|rkts)` 


References:  


*  Racket 2020, The Racket Reference, v.7.8, viewed 17 September 2020, https://docs.racket-lang.org/reference/index.html
*  Racket 2020, 1.3 The Reader, The Racket Reference, v.7.8, viewed 17 September 2020, https://docs.racket-lang.org/reference/reader.html
*  soegaard/racket-highlight-for-github 2014, generate-keywords.rkt & generate-regular-expressions.rkt, github.com, viewed 17 September 2020, https://github.com/soegaard/racket-highlight-for-github
*  mawww/kakoune 2020, scheme.kak, github.com, viewed 17 September 2020, https://github.com/mawww/kakoune/blob/master/rc/filetype/scheme.kak
*  mawww/kakoune 2020, lisp.kak, viewed 28 September 2020, https://github.com/mawww/kakoune/blob/master/rc/filetype/lisp.kak
*  CodeMirror 2018, scheme.js, github.com, viewed 17 September 2020, https://github.com/codemirror/CodeMirror/blob/master/mode/scheme/scheme.js#L46


######  Implemented by an optimist


- [x] binary numbers
- [x] octal numbers
- [x] decimal numbers
- [x] hexadecimal numbers
- [x] extflonum


  
Parenthesis, brackets, braces, and prefix notation highlighting.  
Just change `0:{colour}` to suit your theme  
`add-highlighter -override shared/racket/code/ regex [()\[\]{}] 0:{colour}`    
`add-highlighter -override shared/racket/code/ regex \(([*+-])\h 1:{colour}`  
  
  
```
plug "KJ_Duncan/kakoune-racket.kak" domain "bitbucket.org" config %{
  hook global WinSetOption filetype=racket %{
    require-module racket
    
    add-highlighter -override shared/racket/code/ regex [()\[\]{}] 0:yellow
    add-highlighter -override shared/racket/code/ regex \(([/*+-])\h 1:yellow
  }
}

```
  
Kakoune default colour [codes](https://github.com/mawww/kakoune/blob/master/colors/default.kak):  

* value: red
* type,operator: yellow
* variable,module,attribute: green
* function,string,comment: cyan
* keyword: blue
* meta: magenta
* builtin: default
 
  
```
##
# trampoline.sh
##

#! /usr/bin/env sh
#|
exec racket -e '(printf "Running...\n")' -u "$0" ${1+"$@"}
|#
#lang racket/base

(printf "The above line of output had been produced via\n")
(printf "a use of the `e' flag.\n")
(printf "Given arguments: ~s\n"
        (current-command-line-arguments))

# --------------------------------------------------------------------------------------------------- #
# --------------------------------------------------------------------------------------------------- #

##
# greeting.rkt
##

#! /usr/bin/env racket
#lang racket

(define verbose? (make-parameter #f))

(define greeting
  (command-line
   #:once-each
   [("-v") "Verbose mode" (verbose? #t)]
   #:args
   (str) str))

(printf "~a~a\n"
        greeting
        (if (verbose?) " to you, too!" ""))
        
```
  
The Racket Guide, 2020, '21.2.1 Unix Scripts', 21 Running and Creating Executables, v7.6, viewed 16 Feb 2020, https://docs.racket-lang.org/guide/scripts.html  
  
```
# https://github.com/alexherbo2/auto-pairs.kak 
# --------------------------------------------------------------------------------------------------- #
plug "alexherbo2/auto-pairs.kak" config %{
  hook global WinCreate .* %{ auto-pairs-enable }
  
  # racket quote '()
  # racket quasiquote `()
  # racket prefix notation (< n 3)
  hook global WinSetOption filetype=racket %{
    require-module auto-pairs
    unset-option window auto_pairs
    set-option window auto_pairs ( ) { } [ ] '"' '"'
  }
}

# --------------------------------------------------------------------------------------------------- #
# NEED TO INSTALL RUST
# https://github.com/eraserhd/parinfer-rust
# --------------------------------------------------------------------------------------------------- #
plug "eraserhd/parinfer-rust" do %{
  cargo install --force --path .
  cargo clean
} config %{
  hook global WinSetOption filetype=(clojure|lisp|scheme|racket) %{
    auto-pairs-disable    # alexherbo2/auto_pairs.kak
    manual-indent-disable # alexherbo2/manual-indent.kak
    parinfer-enable-window -smart
  }
}

```  

Rainbow:  


*  jjk96/kakoune-rainbow https://github.com/JJK96/kakoune-rainbow
*  listentolist/kakoune-rainbow https://gitlab.com/listentolist/kakoune-rainbow
*  bodhizafa/kak-rainbow https://github.com/Bodhizafa/kak-rainbow


Incompatible plugins:  


* alexherbo2/manual-indent https://github.com/alexherbo2/manual-indent.kak


---


##### Kakoune General Information #####


Work done? Have some fun. Share any improvements, ideas or thoughts with the community at [discuss.kakoune](https://discuss.kakoune.com/).  
  
Kakoune is an open source modal editor. The source code lives on github [mawww/kakoune](https://github.com/mawww/kakoune#-kakoune--).  
A discussion on the repository structure of _’community plugins’_ for Kakoune can be reviewed here: [Standardi\(s|z\)ation of plugin file-structure layout #2402](https://github.com/mawww/kakoune/issues/2402).  
Read the Kakoune wiki for information on install options via a [Plugin Manager](https://github.com/mawww/kakoune/wiki/Plugin-Managers).  
  
Thank you to the Kakoune community 210+ contributors for a great modal editor in the terminal. I use it daily for the keyboard shortcuts.  
  
  
---
  
That's it for the readme, anything else you may need to know just pick up a book and read it [Polar Bookshelf](https://github.com/burtonator/polar-bookshelf). Thanks all. Bye.
